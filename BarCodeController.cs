﻿using BarcodeModule.Models;
using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BarcodeModule
{
    public class BarCodeController : AppController
    {

        public async Task<ResultItem<string>> GetQRCodeImage(GetQRCodeImageRequest request)
        {
            var taskResult = await Task.Run<ResultItem<string>>(() =>
            {
                var result = new ResultItem<string>
                {
                    ResultState = Globals.LState_Success.Clone()
                };

                if (string.IsNullOrEmpty(request.RootPathForImages))
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Root-Path provided!";
                    return result;
                }

                if (string.IsNullOrEmpty(request.Content))
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Content provided!";
                    return result;
                }

                if (!Directory.Exists(request.RootPathForImages))
                {
                    try
                    {
                        Directory.CreateDirectory(request.RootPathForImages);
                    }
                    catch (Exception ex)
                    {

                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Error while creating root-Directory: {ex.Message}";
                        return result;
                    }
                }

                var fileName = Path.Combine(request.RootPathForImages, $"{Guid.NewGuid().ToString()}.bmp");

                try
                {
                    QRCodeGenerator qrGenerator = new QRCodeGenerator();
                    QRCodeData qrCodeData = qrGenerator.CreateQrCode(request.Content, QRCodeGenerator.ECCLevel.Q);
                    QRCode qrCode = new QRCode(qrCodeData);
                    Bitmap qrCodeImage = qrCode.GetGraphic(20);
                    qrCodeImage.Save(fileName);
                    result.Result = fileName;
                }
                catch (Exception ex)
                {

                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"Error while creating QRCode: {ex.Message}";
                }

                return result;
            });

            return taskResult;

        }

        public BarCodeController(Globals globals) : base(globals)
        {
        }
    }
}
