﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BarcodeModule.Models
{
    public class GetQRCodeImageRequest
    {
        public string RootPathForImages { get; set; }
        public string Content { get; set; }

        public GetQRCodeImageRequest(string rootPathForImages, string content)
        {
            RootPathForImages = rootPathForImages;
            Content = content;
        }
    }
}
